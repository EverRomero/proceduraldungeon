using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SimpleRandomWalkParam_",menuName = "PCG/SimpleRandomWalkSO")]
public class SimpleRandomWalkSO : ScriptableObject
{

    public int iteractionsNumber = 10;

    public int walkLenght = 10;

    public bool startRandomIteration = true;
}
