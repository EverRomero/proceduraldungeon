using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [SerializeField] protected LayerMask layerMask;
    
    protected bool playerTraked;
    protected NavMeshAgent agent;
    protected Collider2D playerCollider;

    private float rangeAttack = 2f;
    protected Vector2 originPosition;
    private float moveSpeed = 3f;

    private void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        originPosition = transform.position;
        agent.autoBraking = false;
        agent.updateRotation = false;
        agent.updateUpAxis = false;
    }
    
    void Update()
    {

        playerCollider = Physics2D.OverlapCircle(transform.position, 10, layerMask);


        if (playerCollider != null)
            TrackedPlayer(playerCollider);

        float distance = Vector2.Distance(originPosition, this.transform.position);
        if (playerCollider == null && playerTraked)
            StartCoroutine(StayOnSite());
        else if (distance > 0.4f && playerCollider == null)
            agent.SetDestination(originPosition);
        else if(distance < 0.4f && playerCollider == null)
            agent.speed = 0;

    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        yield return new WaitForSeconds(1.5f);
        agent.speed = moveSpeed;
        playerTraked = false;
    }

    void TrackedPlayer(Collider2D playerCol)
    {
        GameObject player = playerCol.gameObject;
        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = moveSpeed;
            agent.SetDestination(player.transform.position);
        }
        playerTraked = true;

    }
}
