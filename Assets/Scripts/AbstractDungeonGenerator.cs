using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public abstract class AbstractDungeonGenerator : MonoBehaviour
{

    [SerializeField] 
    protected VisualizeTilemap visualizeTilemap = null;

    [SerializeField] 
    protected Vector2Int startPos = Vector2Int.zero;

    public void GenerateDungeon()
    {
        
        visualizeTilemap.Clear();

        RunProceduralGeneration();

    }

    protected abstract void RunProceduralGeneration();

}
