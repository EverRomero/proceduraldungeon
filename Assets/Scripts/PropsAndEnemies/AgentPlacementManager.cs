using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using NavMeshPlus.Components;
using Unity.VisualScripting;
using UnityEngine;

public class AgentPlacementManager : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyPrefab, playerPrefab;

    [SerializeField] 
    private CinemachineVirtualCamera vCamera;
    
    [SerializeField] 
    private NavMeshSurface navigation;

    
    [SerializeField] 
    private CameraFollowTarget _Camera;
    
    [SerializeField]
    private int playerRoomIndex;

    [SerializeField]
    private List<int> roomEnemiesCount;

    DungeonData dungeonData;
    
    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
    }
    

    public void PlaceAgents()
    {
        if (dungeonData == null)
            return;
        
        navigation.BuildNavMeshAsync();
        
        for (int i = 0; i < dungeonData.rooms.Count; i++)
        {
            Room room = dungeonData.rooms[i];
            RoomGraph roomGraph = new RoomGraph(room.floorTiles);
            
            HashSet<Vector2Int> roomFloor = new HashSet<Vector2Int>(room.floorTiles);
            
            roomFloor.ExceptWith(dungeonData.corridors);

            Dictionary<Vector2Int, Vector2Int> roomMap = roomGraph.RunBFS(roomFloor.First(), room.propPositions);
            
            room.positionsAccessibleFromPath = roomMap.Keys.OrderBy(x => Guid.NewGuid()).ToList();

            
            if (dungeonData.firstRoom.roomCenterPos == room.roomCenterPos)
            {

                playerRoomIndex = i;
                
                GameObject player = Instantiate(playerPrefab);
                player.transform.localPosition = dungeonData.rooms[i].roomCenterPos + Vector2.one*0.5f;
                /*vCamera.Follow = player.transform;
                vCamera.LookAt = player.transform;*/
                _Camera.Target = player.transform;
                dungeonData.PlayerReference = player;
                
              
            }
            
            if(roomEnemiesCount.Count > i && i != playerRoomIndex)
            {
                PlaceEnemies(room, roomEnemiesCount[i]);
            }

            
            
        }
    }
    
    private void PlaceEnemies(Room room, int enemysCount)
    {
        for (int k = 0; k < enemysCount; k++)
        {
            if (room.positionsAccessibleFromPath.Count <= k)
            {
                return;
            }
            GameObject enemy = Instantiate(enemyPrefab);
            enemy.transform.localPosition = (Vector2)room.positionsAccessibleFromPath[k] + Vector2.one*0.5f;
            room.enemiesInTheRoom.Add(enemy);
        }
    }
}

public class RoomGraph
{
    public static List<Vector2Int> fourDirections = new()
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    Dictionary<Vector2Int, List<Vector2Int>> graph = new Dictionary<Vector2Int, List<Vector2Int>>();

    public RoomGraph(HashSet<Vector2Int> roomFloor)
    {
        foreach (Vector2Int pos in roomFloor)
        {
            List<Vector2Int> neighbours = new List<Vector2Int>();
            foreach (Vector2Int direction in fourDirections)
            {
                Vector2Int newPos = pos + direction;
                if (roomFloor.Contains(newPos))
                {
                    neighbours.Add(newPos);
                }
            }
            graph.Add(pos, neighbours);
        }
    }
    
    public Dictionary<Vector2Int, Vector2Int> RunBFS(Vector2Int startPos, HashSet<Vector2Int> occupiedNodes)
    {

        Queue<Vector2Int> nodesToVisit = new Queue<Vector2Int>();
        nodesToVisit.Enqueue(startPos);

        HashSet<Vector2Int> visitedNodes = new HashSet<Vector2Int>();
        visitedNodes.Add(startPos);
        
        Dictionary<Vector2Int, Vector2Int> map = new Dictionary<Vector2Int, Vector2Int>();
        map.Add(startPos, startPos);

        while (nodesToVisit.Count > 0)
        {
            Vector2Int node = nodesToVisit.Dequeue();
            List<Vector2Int> neighbours = graph[node];

            foreach (Vector2Int neighbourPosition in neighbours)
            {
                if (!visitedNodes.Contains(neighbourPosition) && !occupiedNodes.Contains(neighbourPosition))
                {
                    nodesToVisit.Enqueue(neighbourPosition);
                    visitedNodes.Add(neighbourPosition);
                    map[neighbourPosition] = node;
                }
            }
        }

        return map;
    }
}
