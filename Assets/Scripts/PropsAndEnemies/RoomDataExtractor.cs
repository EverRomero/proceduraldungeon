using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDataExtractor : MonoBehaviour
{
    private DungeonData dungeonData;
    
    public GameEvent OnFinishedRoomProcessing;
    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
    }
    
    public void ProcessRooms()
    {
        if (dungeonData == null)
        {
            return;
        }

        foreach (Room room in dungeonData.rooms)
        {
            foreach (Vector2Int tilePosition in room.floorTiles)
            {
                int neighboursCount = 4;

                if(room.floorTiles.Contains(tilePosition+Vector2Int.up) == false)
                {
                    room.nearWallTilesUp.Add(tilePosition);
                    neighboursCount--;
                }
                if (room.floorTiles.Contains(tilePosition + Vector2Int.down) == false)
                {
                    room.nearWallTilesDown.Add(tilePosition);
                    neighboursCount--;
                }
                if (room.floorTiles.Contains(tilePosition + Vector2Int.right) == false)
                {
                    room.nearWallTilesRight.Add(tilePosition);
                    neighboursCount--;
                }
                if (room.floorTiles.Contains(tilePosition + Vector2Int.left) == false)
                {
                    room.nearWallTilesLeft.Add(tilePosition);
                    neighboursCount--;
                }
                
                if (neighboursCount <= 2)
                {
                    room.cornerTiles.Add(tilePosition);
   
                } 

                if (neighboursCount == 4)
                    room.innerTiles.Add(tilePosition);
            }

            room.nearWallTilesUp.ExceptWith(room.cornerTiles);
            room.nearWallTilesDown.ExceptWith(room.cornerTiles);
            room.nearWallTilesLeft.ExceptWith(room.cornerTiles);
            room.nearWallTilesRight.ExceptWith(room.cornerTiles);
        }
        
        OnFinishedRoomProcessing.Raise();
    }
    
}
