using System.Collections;
using System.Collections.Generic;
using NavMeshPlus.Components;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Vector2 = System.Numerics.Vector2;

public class BossPlayerPlacer : MonoBehaviour
{

    
    [SerializeField] 
    private NavMeshSurface navigation;
    
    [SerializeField] private GameObject bossPrefab;
    [SerializeField] private GameObject playerPrefab;

    [SerializeField]
    private CameraFollowTarget cameraBoosFight;
    
    // DATA
    private DungeonData dungeonData;



    private void Awake()
    {

        dungeonData = FindObjectOfType<DungeonData>();
        if (dungeonData == null)
        {
            dungeonData = gameObject.AddComponent<DungeonData>();
        }

    }
    
    public void PlacePlayerAndBoss()
    {
        navigation.BuildNavMeshAsync();
        
        GameObject boss = Instantiate(bossPrefab);
        GameObject player = Instantiate(playerPrefab);

        Light2D lightPlayer = player.GetComponent<Light2D>();

        lightPlayer.pointLightInnerRadius = 4f;
        lightPlayer.pointLightOuterRadius = 6f;
        
        foreach (Vector2Int pos in dungeonData.floorRandomWalk)
        {
            if (pos != dungeonData.startPosRandomWalk)
            {
                player.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            }
            
        }
        
        cameraBoosFight.Target = player.transform;
        boss.transform.localPosition = new Vector3(dungeonData.startPosRandomWalk.x, dungeonData.startPosRandomWalk.y, 0);

    }
    
    
}
