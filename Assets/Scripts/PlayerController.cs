using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Vector2 speed = new Vector2(50, 50);
    
    void Update()
    {

        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");


        Vector3 movement = new Vector3(speed.x * inputX, speed.y * inputY, 0);

        movement *= Time.deltaTime;
        
        transform.Translate(movement);

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("LastRoomTag"))
        {

            SceneManager.LoadScene("BoosFightScene");

        }
    }
}
